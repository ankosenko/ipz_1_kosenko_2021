import http from 'http';
import decodeUriComponent from 'decode-uri-component';
import {
    login,
    registration,
    getUserData,
    getWorking,
    calculateSalary,
    changeSchedule

} from './database.js';


const port = process.env.PORT || 3000;

let users = [];
http.createServer((request, response) => {
    let body = '';
    request.on('data', chunk => {
        body += chunk;
    });
    request.on('end', async () => {
        const func = body.split('=')[0];
        const data = decodeUriComponent(body.split('=')[1]).split('&');
        let resp = '';
        console.log(`Запит: ${func}`);
        if(!!data[0]) {
            console.log(data);
        }
        

        if(func === 'login') {
            if(users.includes(data[0])) {
                resp = 'Сесія на даному аккаунті вже триває.';
            }
            else {
                resp = await login(data[0], data[1], data[2]);
                users.push(data[0]);
            }
        }
        if(func === 'registration') {
            resp = await registration(data[0], data[1], data[2], data[3], data[4], data[5]);
        }
        if(func === 'getUserData') {
            resp = await getUserData(data[0]);
        }
        if(func === 'getWorking') {
            resp = await getWorking(data[0], data[1]);
        }
        if(func === 'calculateSalary') {
            resp = await calculateSalary(data[0], data[1], data[2]);
        }
        if(func === 'changeSchedule') {
            resp = await changeSchedule(data[0], data[1], data[2]);
        }
        if(func === 'logout') {
            const index = users.indexOf(data[0]);
            if (index !== -1) {
                users.splice(index, 1);
            }
            resp = 'true';
        }

        console.log(`Відповідь: ${resp}\n`);
        response.end(resp);
    });
}).listen(port, () => console.log(`Server started on port ${port}`));