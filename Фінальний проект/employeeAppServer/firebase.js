
import firebase from "firebase/compat/app";
import "firebase/compat/database";

const firebaseConfig = {
  apiKey: "AIzaSyAKKomsam4GwjJtUWA_Jy5fpL72DfCJPTo",
  authDomain: "employeedatabase-5e140.firebaseapp.com",
  databaseURL: "https://employeedatabase-5e140-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "employeedatabase-5e140",
  storageBucket: "employeedatabase-5e140.appspot.com",
  messagingSenderId: "226414607854",
  appId: "1:226414607854:web:5c395491d325a983d9a2ff"
};

firebase.default.initializeApp(firebaseConfig);

export async function GET_DATA(path) {
  let response;
  await firebase.database().ref(path).get().then(data => {
      if(data.exists()) {
        response = data.val();
      } 
      else {
        response = false;
      }
  });
  return response;
}

export async function WRITE_DATA(updates) {
  await firebase.database().ref().update(updates);
}


