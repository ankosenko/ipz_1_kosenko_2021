import {GET_DATA, WRITE_DATA} from './firebase.js';


export async function login(id, name, surname) {
    let message = '';
    const data = await GET_DATA(`Users/${id}`);
    if(data) {
        if(data.name === name && data.surname === surname) {
            message = 'true';
        }
        else if(data.name !== name) {
            message = `Ім'я користувача невірне!`;
        }
        else {
            message = `Прізвище користувача невірне!`;
        }
    } 
    else {
        message = 'Користувач з даним ідентифікатором відсутній!';
    }

    return message;
}


export async function registration(name, surname, birth, stage, pos) {
    let id = '';
    const usersId = await GET_DATA(`Users`);
    while(true) {
        id = randomInteger(100000, 999999);
        if(Object.keys(usersId).includes(id)) {
            id = randomInteger(100000, 999999);
        }
        else {
            break;
        }
    }
   

    let updates = {};
    updates[`Users/${id}/name`] = name;
    updates[`Users/${id}/surname`] = surname;
    updates[`Users/${id}/birth`] = birth;
    updates[`Users/${id}/stage`] = stage;
    updates[`Users/${id}/pos`] = pos;
    const arr = shuffle();
    updates[`Users/${id}/Schedule/mon`] = arr[0];
    updates[`Users/${id}/Schedule/tue`] = arr[1];
    updates[`Users/${id}/Schedule/wed`] = arr[2];
    updates[`Users/${id}/Schedule/thu`] = arr[3];
    updates[`Users/${id}/Schedule/fri`] = arr[4];
    updates[`Users/${id}/Schedule/sat`] = arr[5];
    await WRITE_DATA(updates);   
    return id;
}
function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    return `${Math.floor(rand)}`;
}
function shuffle() {
    const arr = ['Денна (6 годин)', 'Денна (8 годин)', 'Нічна (6 годин)', 'Денна (8 годин)', 'Нічна (8 годин)', 'Відмінити зміну'];
    return arr.sort(() => Math.random() - 0.5);
}


export async function getUserData(id) {
    let message = '';
    const data = await GET_DATA(`Users/${id}`);
    if(data) {
        message = `${data.name}&${data.surname}&${data.birth}&${data.pos}&${data.stage}&${data?.workingTime ? parseInt(data.workingTime / 60) : ""}\n`;
        message += `${data.Schedule.mon}&${data.Schedule.tue}&${data.Schedule.wed}&${data.Schedule.thu}&${data.Schedule.fri}&${data.Schedule.sat}`;
    } 

    return message;
}

export async function getWorking(id, time) {
    let updates = {};
    const data = await GET_DATA(`Users/${id}/workingTime`);
    time = Number(time.split(':')[0]) * 60 + Number(time.split(':')[1]);
    if(data) {
        updates[`Users/${id}/workingTime`] = Number(data) + time;
    } 
    else {
        if(time !== 0) {
            updates[`Users/${id}/workingTime`] = time;
        }
    }
    await WRITE_DATA(updates);
    return "true";
}

export async function calculateSalary(id, time1, time2) {
    let message = '';
    const data = await GET_DATA(`Users/${id}/workingTime`);
    if(data) {
        message = Number(data) >= (Number(time1) + Number(time2)) * 60 ? "true" : "false";
    } 
    else {
        message = "true";
    }
    return message;
}

export async function changeSchedule(id, day, schedule) {
    let message = 'true';
    const days = {
        "Понеділок": 'mon',
        "Вівторок": 'tue',
        "Середа": 'wed',
        "Четвер": 'thu',
        "П'ятниця": 'fri',
        "Субота": 'sat',
    }
    let updates = {};
    updates[`Users/${id}/Schedule/${days[day]}`] = schedule;
    await WRITE_DATA(updates).catch(() => message = '');
    return message;
}



