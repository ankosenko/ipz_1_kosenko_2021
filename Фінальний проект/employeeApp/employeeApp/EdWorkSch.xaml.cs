﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Collections.Specialized;

namespace employeeApp
{
    /// <summary>
    /// Interaction logic for EdWorkSch.xaml
    /// </summary>
    public partial class EdWorkSch : Window
    {
        public EdWorkSch()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string day = schlist.Text.Trim();
            string zm = zmlist.Text.Trim();

            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("changeSchedule", Variable.Id + '&' + day + '&' + zm);
                    var response = webClient.UploadValues(Variable.Url, pars);
                    string str = System.Text.Encoding.UTF8.GetString(response);

                    if (str != "")
                    {
                        MessageBox.Show(string.Format("Підтверджено!\nЗмінено розклад в день: {0}.\nЗмінено зміну: {1}", day, zm));
                        MainPage frm = new MainPage();
                        frm.Show();
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Не вдалось змінити розклад.");
                    }
                }
            }
            catch (WebException)
            {
                MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
            }
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainPage frm = new MainPage();
            frm.Show();
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("logout", Variable.Id);
                    var response = webClient.UploadValues(Variable.Url, pars);
                }
            }
            catch (WebException) { }
        }
    }
}
