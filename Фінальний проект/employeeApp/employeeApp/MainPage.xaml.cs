﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Collections.Specialized;

namespace employeeApp
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Window
    {
        public MainPage()
        {
            InitializeComponent();
            string name = "name";
            string surname = "surname";
            string date = "birthdate";
            string pos = "position";
            int wstage = 00;
            int rh = 00;
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("getUserData", Variable.Id);
                    var response = webClient.UploadValues(Variable.Url, pars);
                    string str = System.Text.Encoding.UTF8.GetString(response);

                    if (str != "")
                    {
                        var data1 = str.Split('\n')[0].Split('&');
                        name = data1[0]; 
                        surname = data1[1];
                        date = data1[2];
                        pos = data1[3];
                        wstage = Convert.ToInt32(data1[4]);
                        if(data1[5] != "")
                        {
                            rh = Convert.ToInt32(data1[5]);
                        }

                        var data2 = str.Split('\n')[1].Split('&');
                        mon.Text = data2[0];
                        tue.Text = data2[1];
                        wed.Text = data2[2];
                        thu.Text = data2[3];
                        fri.Text = data2[4];
                        sat.Text = data2[5];
                    }
                    else
                    {
                        MessageBox.Show("Не вдалось отримати дані.");
                    }
                }
            }
            catch (WebException)
            {
                MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
            }

            empname.Text = string.Format("Ім'я та прізвище: {0} {1}", name, surname);
            datebirth.Text = string.Format("Дата народження: {0}", date);
            stage.Text = string.Format("Стаж роботи: {0} років", wstage.ToString());
            position.Text = string.Format("Посада: {0}", pos);
            robhour.Text = string.Format("Кількість відпрацьованих годин: {0}", rh.ToString());
        }

        private void Change_schedule(object sender, RoutedEventArgs e)
        {
            EdWorkSch frm = new EdWorkSch();
            frm.Show();
            Close();
        }

        private void Accounting_workhours(object sender, RoutedEventArgs e)
        {
            Accountingwork frm = new Accountingwork();
            frm.Show();
            Close();
        }

        private void Begin_work(object sender, RoutedEventArgs e)
        {
            Working frm = new Working();
            frm.Show();
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("logout", Variable.Id);
                    var response = webClient.UploadValues(Variable.Url, pars);
                }
            }
            catch (WebException){ }
        }
    }
}
