﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Collections.Specialized;

namespace employeeApp
{
    /// <summary>
    /// Interaction logic for Registration.xaml
    /// </summary>
    public partial class Registration : Window
    {
        public Registration()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = employeeName.Text.Trim();
            if (name == "" || Char.IsUpper(name, 0) == false) {
                employeeName.ToolTip = "Поле введено невірно!";
                employeeName.Background = Brushes.Red;
            }
            else {
                string surname = employeeSurname.Text.Trim();
                if (surname == "" || Char.IsUpper(surname, 0) == false)
                {
                    employeeSurname.ToolTip = "Поле введено невірно!";
                    employeeSurname.Background = Brushes.Red;
                }
                else {
                    var r = new Regex("^\\d\\d-\\d\\d-\\d\\d\\d\\d$");
                    string datebirth = employeeBirth.Text.Trim();
                    if (datebirth == "" || r.IsMatch(datebirth) == false)
                    {
                        employeeBirth.ToolTip = "Поле введено невірно!";
                        employeeBirth.Background = Brushes.Red;
                    }
                    else {
                        string stage = employeeStage.Text.Trim();
                        int st = Int32.Parse(stage);
                        if (stage == "" || st <= 0 || st >= 65)
                        {
                            employeeStage.ToolTip = "Поле введено невірно!";
                            employeeStage.Background = Brushes.Red;
                        }
                        else {
                            string pos = employeePos.Text.Trim();
                            string[] array = { "Помічник", "Стажист", "Дільничий", "Менеджер", "Заступник директора", "Управляючий", "Директор"};
                            if (pos == "" || array.Any(pos.Contains))
                            {
                                employeeName.ToolTip = " ";
                                employeeName.Background = Brushes.Transparent;
                                employeeSurname.ToolTip = " ";
                                employeeSurname.Background = Brushes.Transparent;
                                employeeBirth.ToolTip = " ";
                                employeeBirth.Background = Brushes.Transparent;
                                employeeStage.ToolTip = " ";
                                employeeStage.Background = Brushes.Transparent;
                                employeePos.ToolTip = " ";
                                employeePos.Background = Brushes.Transparent;


                                try
                                {
                                    using (var webClient = new MyWebClient())
                                    {
                                        var pars = new NameValueCollection();
                                        pars.Add("registration", employeeName.Text + '&' + employeeSurname.Text + '&' + employeeBirth.Text + '&' + employeeStage.Text + '&' + employeePos.Text);
                                        var response = webClient.UploadValues(Variable.Url, pars);
                                        string str = System.Text.Encoding.UTF8.GetString(response);
                                        if(str != "")
                                        {
                                            MessageBox.Show(string.Format("Дані введено коректно.\nВаш ідентифікатор працівника: {0}", str));
                                            MainWindow frm = new MainWindow();
                                            frm.Show();
                                            Close();
                                        }
                                        else
                                        {
                                            MessageBox.Show("Сталася помилка при реєстрації.");
                                        }

                                    }
                                }
                                catch (WebException)
                                {
                                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                                }
                            }
                            else
                            {
                                employeePos.ToolTip = "Поле введено невірно!";
                                employeePos.Background = Brushes.Red;
                            }
                        }
                    }
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow frm = new MainWindow();
            frm.Show();
            Close();
        }
    }
}
