﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Net;
using System.Collections.Specialized;

namespace employeeApp
{
    /// <summary>
    /// Interaction logic for Working.xaml
    /// </summary>
    public partial class Working : Window
    {
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        Stopwatch sw = new Stopwatch();
        string currentTime = string.Empty;
        public Working()
        {
            InitializeComponent();
            dispatcherTimer.Tick += new EventHandler(dt_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
        }
        void dt_Tick(object sender, EventArgs e)
        {
            if (sw.IsRunning)
            {
                TimeSpan ts = sw.Elapsed;
                currentTime = String.Format("{0:00}:{1:00}:{2:00}",
                ts.Hours, ts.Minutes, ts.Seconds);
                clocktxtblock.Text = currentTime;
            }
        }
        private void startbtn_Click(object sender, RoutedEventArgs e)
        {
            sw.Start();
            dispatcherTimer.Start();
        }

        private void stopbtn_Click(object sender, RoutedEventArgs e)
        {
            if (sw.IsRunning)
            {
                sw.Stop();
            }
        }
        private void resetbtn_Click(object sender, RoutedEventArgs e)
        {
            sw.Reset();
            clocktxtblock.Text = "00:00:00";
        }

        private void finish_Button(object sender, RoutedEventArgs e)
        {
            if (currentTime == "")
            {
                MessageBox.Show("Вихід без початку роботи здійснено");
                MainPage frm = new MainPage();
                frm.Show();
                Close();
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("getWorking", Variable.Id + '&' + currentTime);
                        var response = webClient.UploadValues(Variable.Url, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);

                        if (str == "true")
                        {
                            MessageBox.Show(string.Format("Ви працювали {0} годин. Дякуємо!", currentTime));
                            MainPage frm = new MainPage();
                            frm.Show();
                            Close();
                        }
                        else
                        {
                            MessageBox.Show("Не вдалось отримати дані.");
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("logout", Variable.Id);
                    var response = webClient.UploadValues(Variable.Url, pars);
                }
            }
            catch (WebException) { }
        }
    }
}
