﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Collections.Specialized;

namespace employeeApp
{
    /// <summary>
    /// Interaction logic for Accountingwork.xaml
    /// </summary>
    public partial class Accountingwork : Window
    {
        public Accountingwork()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string day = zmday.Text.Trim();
            string night = zmnight.Text.Trim();
            if (day == "" || night == "")
            {
                zmday.ToolTip = "Поле введено невірно!";
                zmday.Background = Brushes.Red;

                zmnight.ToolTip = "Поле введено невірно!";
                zmnight.Background = Brushes.Red;
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("calculateSalary", Variable.Id + '&' + day + '&' + night);
                        var response = webClient.UploadValues(Variable.Url, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);

                        if (str == "true")
                        {
                            int resn = Int32.Parse(night) * 2;
                            int accres = (Int32.Parse(day) + resn) * 15;
                            MessageBox.Show(string.Format("Ваша поточна зарплата за цей місяц: {0}$", accres.ToString()));
                            MainPage frm = new MainPage();
                            frm.Show();
                            Close();
                        }
                        else
                        {
                            MessageBox.Show("Введена кількість годин менша за кількість відпрацьованих");
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainPage frm = new MainPage();
            frm.Show();
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("logout", Variable.Id);
                    var response = webClient.UploadValues(Variable.Url, pars);
                }
            }
            catch (WebException) { }
        }
    }
}
