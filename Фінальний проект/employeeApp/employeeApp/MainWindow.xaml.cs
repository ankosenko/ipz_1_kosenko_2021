﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Collections.Specialized;

namespace employeeApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }
       
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string id = employeeID.Password.Trim();
            string name = employeeName.Text.Trim();
            string surname = employeeSurname.Text.Trim();
            if (id.Length != 6)
            {
                employeeID.ToolTip = "Поле введено невірно!";
                employeeID.Background = Brushes.Red;
            }
            else {
                if (Char.IsUpper(name, 0) == false)
                {
                    employeeName.ToolTip = "Поле введено невірно!";
                    employeeName.Background = Brushes.Red;
                }
                else {
                    if (Char.IsUpper(surname, 0) == false)
                    {
                        employeeSurname.ToolTip = "Поле введено невірно!";
                        employeeSurname.Background = Brushes.Red;
                    }
                    else {
                        employeeID.ToolTip = " ";
                        employeeID.Background = Brushes.Transparent;
                        employeeName.ToolTip = " ";
                        employeeName.Background = Brushes.Transparent;
                        employeeSurname.ToolTip = " ";
                        employeeSurname.Background = Brushes.Transparent;

                        try
                        {
                            using (var webClient = new MyWebClient())
                            {
                                var pars = new NameValueCollection();
                                pars.Add("login", employeeID.Password.ToString() + '&' + employeeName.Text + '&' + employeeSurname.Text);
                                var response = webClient.UploadValues(Variable.Url, pars);
                                string str = System.Text.Encoding.UTF8.GetString(response);

                                if(str == "true")
                                {
                                    Variable.Id = employeeID.Password.ToString();
                                    MessageBox.Show("Дані введено коректно. Доброго дня!");
                                    MainPage frm = new MainPage();
                                    frm.Show();
                                    Close();
                                }
                                else
                                {
                                    MessageBox.Show(str);
                                }
                            }
                        }
                        catch (WebException)
                        {
                            MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                        }
                    }
                }
            }
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Registration frm = new Registration();
            frm.Show();
            Close();
        }
    }
}
