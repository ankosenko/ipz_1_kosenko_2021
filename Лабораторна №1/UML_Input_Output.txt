@startuml
start
:Input/Output;
repeat  :User enters login and password;
backward :Invalid login and password;
repeat while (Correct login and password ?) is (No) not (Yes);
:User successfully logs;
:User logs into the systems;
stop
@enduml