﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace employeeApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string id = employeeID.Password.Trim();
            string name = employeeName.Text.Trim();
            string surname = employeeSurname.Text.Trim();

            if (id.Length <= 5)
            {
                employeeID.ToolTip = "Поле введено невірно!";
                employeeID.Background = Brushes.Red;
            }
            else {
                employeeID.ToolTip = " ";
                employeeID.Background = Brushes.Transparent;
                employeeName.ToolTip = " ";
                employeeName.Background = Brushes.Transparent;
                employeeSurname.ToolTip = " ";
                employeeSurname.Background = Brushes.Transparent;

                MessageBox.Show("Дані введено коректно. Доброго дня!");
                MainPage frm = new MainPage();
                frm.Show();
                Close();
            }
        }
    }
}
