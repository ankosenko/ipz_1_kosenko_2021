﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace employeeApp
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Window
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Change_schedule(object sender, RoutedEventArgs e)
        {
            EdWorkSch frm = new EdWorkSch();
            frm.Show();
            Close();
        }

        private void Accounting_workhours(object sender, RoutedEventArgs e)
        {
            Accountingwork frm = new Accountingwork();
            frm.Show();
            Close();
        }

        private void Begin_work(object sender, RoutedEventArgs e)
        {
            Working frm = new Working();
            frm.Show();
            Close();
        }
    }
}
