﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace employeeApp
{
    /// <summary>
    /// Interaction logic for EdWorkSch.xaml
    /// </summary>
    public partial class EdWorkSch : Window
    {
        public EdWorkSch()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string day = schlist.Text.Trim();
            string zm = zmlist.Text.Trim();
            MessageBox.Show("Підтверджено!");
            MainPage frm = new MainPage();
            frm.Show();
            Close();
        }
    }
}
